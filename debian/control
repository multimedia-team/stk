Source: stk
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Arch:
 libasound2-dev [linux-any],
 libjack-dev [linux-any],
 libreadline-dev,
 librtaudio-dev [linux-any],
 librtmidi-dev [linux-any],
Build-Depends-Indep:
 doxygen,
Standards-Version: 4.7.0
Homepage: https://ccrma.stanford.edu/software/stk/
Vcs-Git: https://salsa.debian.org/multimedia-team/stk.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/stk
Rules-Requires-Root: no

Package: libstk-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 librtaudio-dev [linux-any],
 librtmidi-dev [linux-any],
 libstk-5.0.0 (= ${binary:Version}),
 ${misc:Depends},
Description: Sound Synthesis Toolkit (development files)
 The Sound Synthesis Toolkit is a C++ library with implementations
 of several sound synthesis algorithms, starting from Frequency
 Modulation, over Physical Modelling and others. It can be used
 as a library, but it also provides some nice software synthesizers.
 .
 This package provides the development files for the sound synthesis toolkit.

Package: libstk-5.0.0
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sound Synthesis Toolkit
 The Sound Synthesis Toolkit is a C++ library with implementations
 of several sound synthesis algorithms, starting from Frequency
 Modulation, over Physical Modelling and others. It can be used
 as a library, but it also provides some nice software synthesizers.

Package: stk
Architecture: any
Depends:
 libstk-5.0.0 (= ${binary:Version}),
 tk,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Sound Synthesis Toolkit (example applications)
 The Sound Synthesis Toolkit is a C++ library with implementations
 of several sound synthesis algorithms, starting from Frequency
 Modulation, over Physical Modelling and others. It can be used
 as a library, but it also provides some nice software synthesizers.
 .
 This package provides the example files for the sound synthesis toolkit.

Package: stk-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Sound Synthesis Toolkit (documentation)
 The Sound Synthesis Toolkit is a C++ library with implementations
 of several sound synthesis algorithms, starting from Frequency
 Modulation, over Physical Modelling and others. It can be used
 as a library, but it also provides some nice software synthesizers.
 .
 This package contains the documentation for the sound synthesis toolkit.
